/**
 * Старая версия.....
 * 
 */

(function ($) {
    /**
     * Хранит базу данных, которая будет загружена через JSON
     * @type json
     */
    var db;

    /**
     * Посл
     */
    $(document).ready(function () {
        $.getJSON('db.php', function (data) {
            db = data;            
            Create();
        });
    });

    /**
     * Формирует ФОРМУ калькулятора
     * @returns {undefined}
     */
    function Create() {
        var form = $("<form></form>").appendTo(".calc");

        var right = $("<div></div>").addClass("radio");
        right.appendTo(form);

        var rights = [{value: 1, name: 'Физическое лицо', checked: true}, {value: 2, name: 'Юридическое лицо', checked: false}];

        for (i in rights) {            
            right.append("<label><input type=\"radio\" name=\"right\" value=\"" + rights[i].value + "\" " + ((rights[i].checked == true) ? "checked" : "") + "/>" + rights[i].name + "</label>");
        }


        var select_tid = $("<select></select>").attr({class: "form-control", name: "tid"}).appendTo(form);
        // Типы транспорта
        $.each(db.type, function () {
            $("<option></option>").val(this.id).text(this.name).appendTo(select_tid);
        });
        select_tid.after("<div id=\"use\"></div>");
        select_tid.change(function () {
            var $this = $(this);            
            for (i in db.use_type) {
                if ($this.val() == db.use_type[i].tid) {
                    $("#use").html("<div class=\"checkbox\"><label><input type=\"checkbox\" name=\"use_type\" value=\"" + db.use_type[i].id + "\">" + db.use_type[i].name + "</label></div>");
                }
            }
        }).change();


        // Строим значение для select region 
        var select_rid = $("<select></select>").attr({class: "form-control", name: "cid"}).appendTo(form);

        $.each(db.region, function () {
            var optgroup = $("<optgroup></optgroup>").attr({label: this.name, id: this.id}).appendTo(select_rid);
            var rid = this.id;
            $.each(db.city, function () {
                if (rid == this.rid) {
                    var option = $("<option></option>").val(this.id).text(this.name).appendTo(optgroup);
                }
            });
        });
        
        var out = "#result .sum"; 
        
        form.append("<div id=\"result\"><div> " +
                "<p>Стоимость страховки</p>" +
                "<span class=\"sum\"></span><span class=\"rub\">&nbsp;pуб.</span>" +
                "</div></div>");                

        form.change(function () {
            var params = $(this).serializeArray();
            $(out).html(Calc(params));
        }).change();
        
    }

    /**
     * Производит расчет стоимости страховки
     * @param {type} params
     * @returns {result|Number}
     */
    function Calc(params) {
        params = normalized(params);
        var f = GetFactory(params.cid, params.right);
        var r = GetRate(params.tid, params.use_type);
        var s = f * r;

        result = Math.ceil(s);
        return result;
    }

    /**
     * Нормализация данных от SerializeArray -> Object JSON
     * Нужна для более простого обращения к значения полей. 
     * @param {json array objects} params
     * @returns {json object}
     */
    function normalized(params) {
        var result = {
            tid: 0,
            cid: 0,
            use_type: 0,
            right: 0
        }

        for (i in params) {
            if (params[i].name in result) {
                result[params[i].name] = parseInt(params[i].value);
            }
        }

        return result;
    }

    /**
     * Производит выборку из таблицы территориальных коэфициентов
     * @param {integer} cid
     * @param {integer} right
     * @returns {integer|Boolean}
     */
    function GetFactory(cid, right) {
        var result = false;
        $.each(db.factor, function () {
            if ((this.cid == cid) && (this.right == right)) {
                result = this.rate;
                return false;   // break функции each
            }
        });
        return result;
    }

    /**
     * Выборка из таблицы тарифов
     * @param {integer} tid
     * @param {integer} use
     * @returns {integer|Boolean}
     */
    function GetRate(tid, use) {
        var result = false;
        $.each(db.baserate, function () {
            if ((this.tid == tid) && (this.use == use)) {
                result = this.price;
                return false;   // break функции each
            }
        });
        return result;
    }

})(jQuery);
